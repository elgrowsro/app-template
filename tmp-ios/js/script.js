$(document).ready(function(){
    $(".dropdown-button").dropdown();
    $(".button-collapse").sideNav();
    $('select').material_select();
     $('ul.tabs').tabs();
    $('.owl-carousel').owlCarousel({
        items:1,
        loop: 'true',
        autoplay:'true'
    });
    $('.image-carousel').owlCarousel({
        items:1,
        loop: 'true',
        autoplay:'true'
    });
    //Новости
    $('.news-list>div').click(function(){
        $('#news').addClass('active');
    });
    //Категории
    $('.categories-list>div').click(function(){
        $('#categories-level-2').addClass('active');
    });
    //Продукт-лист
    $('.caregory-2-list>div').click(function(){
        $('#product-list').addClass('active');
    });
    //Продукт
    $('.products-table>div>a').click(function(){
        $('#product').addClass('active');
    });
    //Фильтр
    $('#filter').click(function(){
        $('#filter-page').addClass('active');
    });
    //Чекбоксы фильтра
    $('.checkboxes-page-href').click(function(){
        $('#filter-checkboxes').addClass('active');
    });
    //Цена фильтр
    $('.price-page-href').click(function(){
        $('#filter-price').addClass('active');
    });
    //Отзывы
    $('#showAll-reviews').click(function(){
        $('#reviews').addClass('active');
    });
    
    $('.back').click(function(){
        $(this).parents('.slide-page').removeClass('active');
    });
    
    $('.sorted>div>div').click(function(){
        $('.sorted>div>div').removeClass('active');
        $(this).addClass('active');
    });
    $('.count-select span').click(function(){
        var val = $(this).siblings('.input-count').text();
        if ($(this).hasClass('plus')){
            val++;
            $(this).siblings('.input-count').text(val);
        }else if ($(this).hasClass('minus')){
            if (val>1){
                val = val -1;
                $(this).siblings('.input-count').text(val);
            }
        }
    });
    $('.slide-button').click(function(){
        $(this).toggleClass('active');
        $(this).siblings('.inner').find('.slided').toggleClass('active');
    });
    //Клики по главному меню
    $('.main-menu li a').click(function(){
          setTimeout(function(){
              $('.button-collapse').sideNav('hide');
          },10);
        var a = $(this).attr('href');
        //Главная
        if (a == "#index-bt"){
            $('.slide-page').removeClass('active');
            $('.tabs-products li').eq(0).find('a').trigger('click');
        }
        //Каталог
        else if (a == "#catalog"){
            $('.slide-page').removeClass('active');
            $('.tabs-products li').eq(1).find('a').trigger('click');
        }
        //Новости
        else if (a == "#news"){
            $('.slide-page').removeClass('active');
            $('.tabs-products li').eq(2).find('a').trigger('click');
        }
        //Доставка и оплата
        else if (a == "#shipping-payment"){
            $('.slide-page').removeClass('active');
            $('#shipping-payment').addClass('active');
        }
        //О приложении
        else if (a == "#info"){
            $('.slide-page').removeClass('active');
            $('#info-page').addClass('active');
        }
        else if (a == "#auth"){
            $('.slide-page').removeClass('active');
            $('#auth').addClass('active');
        }
        return false;
    });
    //Поиск
    $('.seacrh-icon').click(function(){
        $('#search').addClass('active');
        $('nav .title-menu .input-field input').focus();
    });
    //Информация о заказе
    $('#history-payments>div>div').click(function(){
        $('#zakaz-info').addClass('active');
    });
    //Личный кабинет
    $('#log-in').click(function(){
        $('#personal-cab').addClass('active');
    });
    //Корзина
    $('.cart-icon').click(function(){
        $('#cart-page').addClass('active');
    });
    $('.next-step-cart').click(function(){
        $('#cart-page-2').addClass('active');
    });
    $('#clear-seacrh').click(function(){
        $('nav .title-menu .input-field input').val('').focus();
    });
    $('.next-step-cart-2').click(function(){
        $('.step-cart-1').slideUp();
        $('.step-cart-2').slideDown();
    });
    $('#type-shipping').change(function(){
        var val = $(this).find(':selected').attr('data');
        $('.dependent').slideUp(200);
        $('#'+val).slideDown(200);
    });
    //Share
    $('.share-icon').click(function(){
        $(this).parents('.slide-page').find('.share').addClass('active');
    });
    $('.share-overlay').click(function(){
        $(this).parents('.share').removeClass('active');
    });
    //Modal
     $('.modal-trigger').leanModal();
    
    
    $('.to-registrate').click(function(){
        $('.register-box').slideDown();
        $('.login-box').slideUp();
    });
    $('.to-login').click(function(){
        $('.register-box').slideUp();
        $('.login-box').slideDown();
    });
    
    
    function preventSelection(element){
        var preventSelection = false;
        
        function addHandler(element, event, handler){
            if (element.attachEvent) 
                element.attachEvent('on' + event, handler);
            else 
                if (element.addEventListener) 
                    element.addEventListener(event, handler, false);
        }
        function removeSelection(){
            if (window.getSelection) { window.getSelection().removeAllRanges(); }
            else if (document.selection && document.selection.clear)
                document.selection.clear();
        }
        function killCtrlA(event){
            var event = event || window.event;
            var sender = event.target || event.srcElement;
            
            if (sender.tagName.match(/INPUT|TEXTAREA/i))
                return;
            
            var key = event.keyCode || event.which;
            if (event.ctrlKey && key == 'A'.charCodeAt(0))  // 'A'.charCodeAt(0) можно заменить на 65
            {
                removeSelection();
                
                if (event.preventDefault) 
                    event.preventDefault();
                else
                    event.returnValue = false;
            }
        }
        
        // не даем выделять текст мышкой
        addHandler(element, 'mousemove', function(){
            if(preventSelection)
                removeSelection();
        });
        addHandler(element, 'mousedown', function(event){
            var event = event || window.event;
            var sender = event.target || event.srcElement;
            preventSelection = !sender.tagName.match(/INPUT|TEXTAREA/i);
        });
        
        // борем dblclick
        // если вешать функцию не на событие dblclick, можно избежать
        // временное выделение текста в некоторых браузерах
        addHandler(element, 'mouseup', function(){
            if (preventSelection)
                removeSelection();
            preventSelection = false;
        });
        
        // борем ctrl+A
        // скорей всего это и не надо, к тому же есть подозрение
        // что в случае все же такой необходимости функцию нужно 
        // вешать один раз и на document, а не на элемент
        addHandler(element, 'keydown', killCtrlA);
        addHandler(element, 'keyup', killCtrlA);
    }
    preventSelection(document);
    
    
    
    
     $(".cart-list>div").swipe( {
        //Generic swipe handler for all directions
        swipe:function(event, direction, distance, duration, fingerCount, fingerData) {
            if (direction == "left"){
                $(this).addClass('swipped-left');  
            }else if (direction == "right"){
                $(this).removeClass('swipped-left');  
            }
        },
        //Default is 75px, set to 0 for demo so any distance triggers swipe
         threshold:0
      });
});









